// importing the "http" module
const http = require("http");

// storing the 4000 in a variable called port
const port = 4000;

//storing the createServer method inside the server variable
const server = http.createServer((request,response) => {
	/*
		create a route that will send the response "Data retrieved from the database" with 200 status code and text plain content type
	*/
	// HTTP method can be acced via "method" property inside the "request" object; they are to be written in ALL CAPS

	// GET REQUEST
	if (request.url === "/items" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database.");
	};

	// POST REQUEST
	// "POST" means that we will be adding/creating information
	if (request.url === "/items" && request.method === "POST") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database.");
	};

});
/*
	GET method is one of the HTTPS methods that we weill be using from this point.
		GET method means that we will be retrieving or reading information
*/

/*
	POSTMAN
		- since the Node.js application that we are building is a backend application, and there is no frontend application yet to process the requests, we will be using postman to process the request.
*/


// using server and port variables
server.listen(port);

//confrimation that the server is running
console.log(`Server now running at port: ${port}`);

// npx kill-port <port> - used to terminate the port activity; useful in cases where the terminal is close without terminating the port processes